package restassured.reqres;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class DeleteRequestTest {

	// @author Aditya Garg
	// https://reqres.in/api
	
	@Test
	public void DeleteUser()
	{   
		
		RestAssured.baseURI = "https://reqres.in/api/users/4";
		RequestSpecification httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
	
		httpRequest.header("Content-Type", "application/json");
	
		Response response = httpRequest.delete();
	 
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 204);
		System.out.println(response.body().asString());
	}


}