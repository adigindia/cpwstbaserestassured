package restassured.reqres;

import static org.testng.Assert.assertEquals;

import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.*;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

// For command line execution
// mvn test -Dtest=GetRequestTest

public class GetRequestTest {
	@Test
	public void GetUserDetails()
	{   
		
		 RestAssured.baseURI = "https://reqres.in/api";
		 RequestSpecification httpRequest = RestAssured.given();
		 Response response = httpRequest.get("/users/2");
		 
		 // Get the status code from the Response. In case of 
		 // a successfull interaction with the web service, we
		 // should get a status code of 200.
		 int statusCode = response.getStatusCode();
		 
		 // Assert that correct status code is returned.
		 Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");

		 response = httpRequest.get("/users");
		 
		 response.prettyPrint();
		 

	}
}