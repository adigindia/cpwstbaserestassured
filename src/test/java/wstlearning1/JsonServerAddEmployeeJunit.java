package wstlearning1;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import org.json.simple.JSONObject;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class JsonServerAddEmployeeJunit {

	@Ignore
	@Test
	public void test1() {

		RestAssured.baseURI = "http://164.52.192.77:3000"; 

		RequestSpecification req = RestAssured.given();
		req.header("Content-type", "application/json");

		JSONObject requestParams = new JSONObject();

		requestParams.put("name", "NewNameRA2"); 
		requestParams.put("salary", "10000");

		req.body(requestParams.toJSONString());

		Response res = req.post("/employees");

		res.prettyPrint();
		int statusCode = res.getStatusCode();

		System.out.println("Status Code = " + statusCode);

		//Unit assertTrue
		assertTrue("Status codes match",201==statusCode);

		// get the JSON response and extract the id created

		JsonPath jsonRes = new JsonPath(res.body().asString()); 

		int id = jsonRes.get("id");

		System.out.println("New Id that has been created = " + id);

	} // end of test1()


	// Get all the names of employees
	@Test
	public void test2() {

		RestAssured.baseURI = "http://164.52.192.77:3000/"; 

		RequestSpecification req = RestAssured.given();
		req.header("Content-type", "application/json");

		JSONObject requestParams = new JSONObject();


		Response res = req.get("/employees");

		int statusCode = res.getStatusCode();

		System.out.println("Status Code = " + statusCode);

		//Unit assertTrue
		assertTrue("Status codes match",200==statusCode);

		// get the JSON response and extract the id created

		JsonPath jsonRes = new JsonPath(res.body().asString()); 

		List<String> allNames = jsonRes.get("name");
		
		for (int i=0;i<allNames.size();i++) {
			
			System.out.println("Name:" + i + "=" + allNames.get(i));
			
		}

	} // end of test2()

}
